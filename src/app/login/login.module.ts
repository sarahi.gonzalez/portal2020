import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatStepperModule} from '@angular/material/stepper';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';


@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    LoginRoutingModule,
    
  ],
  exports:[
    MatStepperModule
  ]
})
export class LoginModule { }
